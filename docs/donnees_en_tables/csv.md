# Traitement des données en tables

## Présentation
L'informatique permet de traiter des quantités d'information très importantes dans des domaines très variés.   
Pour s'en convaincre on peut regarder le site [www.data.gouv.fr](www.data.gouv.fr/fr/).  
Encore faut-il que ces données soient organisées pour pouvoir les exploiter.

Plusieurs formats de fichier existent dont le format en tables de type CSV, objet de ce cours.   
En terminale NSI, on découvrira les base de données qui sont destinés aussi à traiter un grand nombre de données.

## Qu'est ce qu'un fichier CSV
![Table CSV](./img/table_csv.png){style="width:60%; margin-left:20%;"}  

CSV signifie **Comma-Separated Values** autrement dit :  *Valeurs séparées par des virgules*.  
Ce format permet d'organiser les données en table dans un fichier :    

- Chaque ligne du fichier correspond à un **enregistrement**,  
- Pour chaque ligne, les **descripteurs** (valeurs) sont séparées par des virgules.  

En réalité les anglo-saxons ont choisi la virgule comme séparateur car leur nombre décimaux sont décrits avec un point (*dot* en anglais).  
En France il n'est pas rare d'opter pour le point-virgule, mais d'autres caractères peuvent ête utilisés.  

La table ci-dessus est donc décrite de la manière suivante : 
``` csv
	Prénom,Nom,Age
	Alain,Térieur,50
	Paul,Auchon,94
	Jean,Raffole,25
```

!!! question "Quel est le séparateur de champs dans ce cas ?"
	Autre exemple de fichier CSV : une liste des villes du monde:
	``` csv
		Id;Name;Latitude;Longitude;Country_ISO;Country_Name;Population
		3040051;les Escaldes;42.50729;1.53414;AD;Andorra;15853
		3041563;Andorra la Vella;42.50779;1.52109;AD;Andorra;20430
		290594;Umm Al Quwain City;25.56473;55.55517;AE;United Arab Emirates;62747
		291074;Ras Al Khaimah City;25.78953;55.9432;AE;United Arab Emirates;351943
		292223;Dubai;25.07725;55.30927;AE;United Arab Emirates;2956587
	```
	
On peut ouvrir les fichiers CSV avec un tableur et traiter les données avec celui-ci:  

![Tableur](./img/tableur_pays.png){"style="width:60%; margin-left:20%;"}

Mais pour plus de souplesse dans le traitement des données, on va programmer en Python.  

Pour la suite on utilise des bibliothèques `csv` et `pandas` qui sont spécialisées dans le traitement des fichier CSV.

