## 1) Les variables en python.
!!! info "Définition :"
	Une **variable** est un espace mémoire qui contient une valeur (on parle d'objets, "objects" en anglais) : un nombre, une chaîne de caractères, ou encore des formes plus complexes...  
	On parle d'**affectation** de variable lorsque l'on change la valeur de la variable.  
	En python l'**affectation** se fait avec le symbole **=**.

![représentation des variables a et b](./img/variables.png){ align=right }
On peut se représenter mentalement une variable comme une boîte pouvant contenir des objets et munit d'une étiquette : son nom (ou identifiant).  
Ici deux variables `a` et `b`.
La variable `a` est vide et la variable `b` pointe vers la case mémoire contenant le nombre entier `3` :

![affectation de variables](./img/affectation.png){ align=center }

## 2) Types de variables et opérations de base
!!! info "Définition :"
	Chaque variable possède un type qui caractérise sont contenu :
	
	- booléen : **`bool`**,
	- nombre entier : **`int`**,
	- nombre décimal : **`float`**,
	- chaîne de caractères : **`str`**.  
	
	L’affectation d’une valeur à une variable définit le type de cette variable.  
	
	Les opérations entre des variables de types différents ne sont pas permises (il existe des conversions automatiques dans certains cas).  
	Par exemple : ```x = 2 + 3.0```  
	`2` est de type `int`, il est converti en `float` pour pouvoir faire l’addition avec `3.0` qui de type `float`.
	`x `sera de type `float`.

!!! note "Remarque :"
	Le résultat d'une opération sur des variables dépend du type des variables :
	
	- `2 + 2` renvoie `4`, alors que
	- `"2" + "2"` renvoie `"22"`.

!!! example "Exemples d’affectations de variables de différents types :"
	``` python linenums="1"
	dimensions_valides = True    # de type bool
	score = 203                  # de type int
	longueur = 1.25              # de type float
	nom_joueur_1 = "mona-lisa"   # de type str
	```

### 2.1) Les opérateurs arithmétiques.
Ils effectuent des opérations sur les nombres entiers (integer) ou décimaux (float).

| Opérateur      | Description                          |
| ----------- | ------------------------------------ |
| **`+`**, **`-`**, **`*`** et **`/`**       | Respectivement : l'addition, la soustraction, la multiplication et la division  |
| **`**`**       | Calcule la puissance : `2**4` renvoie `16` |
| **`//`**    | **Division entière** : renvoie le résultat de la division arrondi à l'entier inférieur |
| **`%`**      | **Opérateur modulo** : renvoie le reste de la division entière. |


### 2.2) Les opérateurs booléens.
Ils effectuent des tests qui renvoient un booléen : vrai (`True`) ou faux (`False`).

| Opérateur      | Description                          |
| ----------- | ------------------------------------ |
| **`==`**      | `a == b` : `a` est-il égal à `b` ?         |
| **`!=`**       | `a != b` : `a` est-il différent de `b` ?  |
| **`<`**, **`<=`**, **`>`** et **`>=`**       | Respectivement : inférieur, inférieur ou égale, supérieur et supérieur ou égal  |
| **`not`**       | `not(a)` : renvoie `True` si `a` vaut `False` et inversement |
| **`and`**    | `a and b` : renvoie `True` si `a` **ET** `b` sont vrais. |
| **`or`**      | `a or b` : renvoie `True` si `a` **OU** `b` sont vrais. |


### 2.3) Les opérateurs sur les chaînes de caractères (string).

| Opérateur      | Description                          |
| ----------- | ------------------------------------ |
| **`'Taratata'`**      | Définition d'une chaîne de caractère sur une ligne        |
| **`"C'est bon."`**       | Autre possibilité de définition d'une chaîne de caractère sur une ligne  |                          |
| **`"Tara" + "tata"`**      | **Concaténation** : renvoie `"Taratata"` |
| **`"ha * 4"`**      | **Répétition** : renvoie `"hahahaha"` |

Il est possible de définir une chaîne de caractères sur plusieurs lignes:

``` python linenums="1"  
chaine1 = 	''' Pour définir une chaîne de caractères sur plusieurs lignes,
		      il suffit de placer trois simple-quote avant et après la chaîne de caractères
			'''
chaine2 = 	""" Ca marche également avec trois double-quote.
			Pratique !
			"""
```
