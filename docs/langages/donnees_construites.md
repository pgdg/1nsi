## 1) Compléments sur les chaînes de caractères de type `str`.
### 1.1) Accès aux caractères d'une chaîne de caractères (`str`).
Pour accéder à un caractère d'une chaîne de caractères, on place l'indice du caractère voulu entre crochet.  
L'indice commence à 0, un indice négatif compte à partir de la fin.
!!! example "Accès aux caractères d'une chaîne de caractères (`str`)"
    ``` python linenums="1"
    >>> chaine = "ma belle chaîne"
    >>> print("Première lettre : ", chaine[0])
    Première lettre : m
    >>> print("Dernière lettre : ", chaine[-1])
    Dernière lettre : e
    >>> print("Quatrième lettre : ", chaine[3])
    Dernière lettre : b
    ```

On peut extraire une tranche de la chaîne de caractères (slice en anglais), toujours en utilisant les crochets.
Dans la tranche [n,m], le n-ième caractère est inclus, mais pas le m-ième.
!!! example "Accès à une tranche d'une chaîne de caractères (`slice`)"
    ``` python linenums="1"
    >>> print("Les deux premières lettres :", chaine[:2])
    Les deux premières lettres : ma
    >>> print("Les six dernières lettres :", chaine[-6:])
    Les six dernières lettres : chaîne
    >>> print("Une partie de la chaîne :", chaine[3:8])
    Une partie de la chaîne : belle
    ```
!!! bug "Attention : Les chaînes sont non modifiables (immuables), on ne peut pas modifier le contenu d’une chaîne de caractères."
    ``` python linenums="1"
    >>> texte = "bonjour à tous"
    >>> texte[0] = "B"
    TypeError: object doesn’t support item assignment.
    ```

### 1.2) Quelques fonctions s’appliquant aux chaîne de caractères.
!!! example "Quelques fonctions s’appliquant aux chaîne de caractères."
    ``` python linenums="1"
    >>> type('mot')
    str
    >>> len('mot')
    3
    >>> min('variable')
    ’a’
    >>> max('variable')
    ’v’
    >>> int('5') # Transtypage en nombre entier
    5
    ```

### 1.3) Quelques méthodes pour les chaînes de caractères.
Une **méthode** est une fonction propre à un objet, ici les `str`.  
La syntaxe pour utiliser une méthode est : **`nom_du_str.nom_de_la_méthode`**
!!! example "Exemple : utilisation de la méthode `count`"
    ``` python linenums="1"
    >>> 'voici une phrase'.count('i') # Pour compter des occurences
    2
    ```
La méthode count() s’applique à l’objet ’voici une phrase’ qui est de type str. ’i’ est une argument de
cette méthode.

Depuis Python 3.6, un nouvel outils permet de formater facilement les chaînes de caractères : les **`f-Strings`** :
!!! example "Exemple d'utilisation des `f-string`."
    ``` python linenums="1"    
    >>> nom = 'Éric'
    >>> age = 49
    >>> print(f"Je suis {nom}, je vais avoir {age} ans.")
    Je suis Éric, je vais avoir 49 ans.
    >>> 'pour écrire en majuscules'.upper()
    POUR ÉCRIRE EN MAJUSCULES
    >>> 'EN mINuscules'.lower()
    en minuscules
    ```

On peut également savoir si une chaîne est présente dans la chaîne de caractères.
!!! example "Savoir si un caractère ou une sous-chaîne est présente dans une chaîne de caractères."
    ``` python linenums="1"    
    >>> 'e' in 'mot'
    False
    >>> 'er' in 'calculer'
    True
    ```

## 2) Types séquentiels : `list`, `tuple``.
### 2.1) Les séquences muables de type `list`
Python possède des types composés qui sont utilisés pour regrouper ensemble des variables de différents types.
Le plus polyvalent est le tableau dynamique (de type `list` ), qui peut être écrit comme une suite de valeurs
séparées par des virgules le tout encadré par des crochets.
!!! example "Exemple de `list`."
    ``` python linenums="1"    
    >>> liste = [] # La liste vide
    >>> len(liste)
    0
    >>> liste2 = [3, 'e', 3.9] # Une liste de trois éléments de type différent
    ```

Comme pour les chaînes de caractères, on peut accéder aux éléments d’une liste à l'aide de crochets. Le premier élément est celui
d’indice 0 :
!!! example "Accès aux éléments d'une `list`."
    ``` python linenums="1"    
    >>> liste2[0]
    3
    >>> liste2[-1] # Le dernier
    3.9
    ```

On peut modifier (on dit qu’une liste est muable) un élément de la liste à l’aide d’une affectation :
!!! example "Modification des éléments d'une `list`."
    ``` python linenums="1"    
    >>> liste2[1] = 7
    >>> print(liste2)
    [3, 7, 3.9]
    >>> [1,2] + [3,4] # Pour concaténer deux listes
    [1, 2, 3, 4]
    ```
!!! example "Quelques méthodes importantes sur les `list`."
    ``` python linenums="1"    
    >>> liste2.append(1) # Ajoute un élément en fin de liste
    >>> print(liste2)
    [3, 7, 3.9, 1]
    >>> p = liste2.pop() # Supprime et renvoie le dernier élément de la liste
    >>> print(p, " et ", liste2)
    1 et [3, 7, 3.9]
    >>> liste2.reverse() # Renverse la liste mais ne renvoie rien
    >>> liste2
    [3.9, 7, 3]
    ```

### 2.2) Les séquences immuables de type `tuple`
Les tuples (n-uplets en français) sont des séquences immuables, généralement utilisées pour stocker des collections
de données hétérogènes. On le définit par une suite de valeurs séparées par des virgules le tout encadré par des
parenthèses.

On ne peut donc pas modifier les éléments d'un ``tuple`, la différence des `list`.
!!! example "Exemple d'utilisation des `tuples`."
    ``` python linenums="1"    
    >>> t = () # Tuple vide
    >>> type(t)
    tuple
    >>> len(t)
    0
    >>> t1 = ("3", [1, 2], 2e7)
    >>> t1[0]
    '3'
    >>> t1[1]
    [1, 2]
    >>> t1[1][1]
    2
    >>> t1[0] = 1 # Un tuple est immuable, cette instruction renvoie une erreur
    ---------------------------------------------------------------------------
    TypeError Traceback (most recent call last)
    <ipython-input-67-c0c4497a27ce> in <module>
    ----> t1[0] = 1 # Un tuple est immuable, cette instruction renvoie une erreur
    TypeError: ’tuple’ object does not support item assignment
    ```

### 2.3) Parcourir les éléments d’une `list`, d’un `tuple` ou d’un objet d'un `str`.
Il existe deux manières d’itérer (ou de parcourir) les éléments de ce type d’objets :

- En itérant sur les indices des éléments : 
<table>
<tr>
<th>Pour les <code><strong>list</code></strong></th><th>Pour les <code><strong>tuple</code></strong></th><th>Pour les <code><strong>str</code></strong></th>
</tr>
<tr>
<td>
```python
>>> liste = [7, 8, 1]
>>> for i in range(len(liste)):
print(liste[i], end = ' ')
7 8 1
```
</td>
<td>
```python
>>> _tuple = ('e', 8, [1])
>>> for i in range(len(_tuple)):
print(_tuple[i], end = ' ')
e 8 [1]
```
</td><td>
```python
>>> chaine = 'str'
>>> for i in range(len(chaine)):
print(chaine[i], end = ' ')
s t r
```
</td>
</tr>
</table>

- En sélectionnant chaque élément de la liste :
<table>
<tr>
<th>Pour les <code><strong>list</code></strong></th><th>Pour les <code><strong>tuple</code></strong></th><th>Pour les <code><strong>str</code></strong></th>
</tr>
<tr>
<td>
```python
>>> liste = [7, 8, 1]
>>> for element in liste:
print(element, end = ' ')
7 8 1
```
</td>
<td>
```python
>>> _tuple = ('e', 8, [1])
>>> for element in _tuple:
print(element, end = ' ')
e 8 [1]
```
</td><td>
```python
>>> chaine = 'str'
>>> for element in chaine:
print(element, end = ' ')
s t r
```
</td>
</tr>
</table>

## 3) Récapitulatif des opérations sur les tableaux dynamiques (type `list`).

| Opération | Résultat | 
| -----: | :-----------   | 
| `s[i] = x` | élément d’index `i` de s est remplacé par `x`| 
| `s[i:j] = t` | tranche de `s` de `i` à `j` est remplacée par le contenu de l’itérable `t`| 
| `del s[i:j]` | identique à `s[i:j] = []` | 
| `s.append(x)` | ajoute `x` à la fin de la séquence (identique à `s[len(s):len(s)] = [x]`)| 
| `s.copy()` | crée une copie superficielle de `s` (identique à `s[:]`)| 
| `s.extend(t)` ou `s = s + t`|  étend `s` avec le contenu de `t`| 
| `s.insert(i, x)` | insère `x` dans `s` à l’index donné par `i`| 
| `s.pop(i)` | récupère l’élément à `i` et le supprime de `s`| 
| `s.remove(x)` | supprime le premier élément de `s` pour lequel `s[i]` est égal à `x`| 
| `s.reverse()` | inverse sur place les éléments de `s`| 
