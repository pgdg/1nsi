## 1) Les compréhensions de `list`.
!!! info "Syntaxe des compréhensions de `list`"
    Les compréhensions de listes permettent d’initialiser facilement des listes. La syntaxe est :  
    `[`**expression1** `for` **element** `in` **liste** `if` **expression2**]  

    - Applique l’**expression1** à chaque élément de la liste si **expression2** est vraie
    - Il peut y avoir 0 ou plus instructions for ou if
    - Si l’expression évalue un tuple, il doit être entre parenthèses

!!! Example "Exemple d'utilisation :"
    ``` python linenums="1"
    >>> vec = [2, 4, 6]
    >>> [3*x for x in vec]
    [6, 12, 18]

    >>> [3*x for x in vec if x > 3]
    [12, 18]

    >>> [3*x for x in vec if x < 2]
    []

    >>> [[x, x**2] for x in vec]
    [[2, 4], [4, 16], [6, 36]]

    >>> [x, x**2 for x in vec]
    # error – il faut des () pour les tuples    
    ```

## 2) Les listes de listes.
Chaque élément d'une liste peut être une liste, on parle de liste de listes.  
Voici un exemple de liste de listes :  
`#!python m = [[1, 3, 4], [5 ,6 ,8], [2, 1, 3], [7, 8, 15]]`  

Le premier élément de la liste ci-dessus est bien une liste (`#!python [1, 3, 4]`), le deuxième élément est aussi une liste (`#!python [5, 6, 8]`)...  
Il est souvent plus pratique de présenter ces " listes de listes" comme suit :  
```python linenums="1"
m = [[1, 3, 4],`  
     [5, 6, 8],`    
     [2, 1, 3],`  
     [7, 8, 15]]`   
```
Nous obtenons ainsi quelque chose qui ressemble beaucoup à un "objet mathématique" très utilisé : une matrice.   

Il est évidemment possible d'utiliser les indices de position avec ces "listes de listes". Pour cela nous allons considérer
notre liste de listes comme une matrice, c'est à dire en utilisant les notions de "ligne" et de "colonne".
Ainsi, pour accéder à l’élément de la 2ème ligne (indice 1), 3ème colonne (indice 2), il faut écrire :  

`#!python a = m[1][2] # après exécution de l’instruction, a = 8`

Il est possible de parcourir l'ensemble des éléments d'une matrice à l'aide de 2 boucles `for` imbriquées :
```python linenums="1" 
for ligne in range(len(m)):
    for colonne in range(len(m[0])):
        a = m[ligne][colonne]
        print(a)
```

On pourra par exemple représenter les pixels d’une image sous forme de matrice et accèder à chacun des pixels à l’aide
de ces 2 boucles imbriquées.

## 3) Les dictionnaires.
Les dictionnaires permettent de "stocker" des données sous forme de 2 éléments **clef** et **valeur** (en anglais : **key** et **value**).  
!!! example "Exemple de dictionnaire :"
    ```python linenums="1"
    >>> dict = {'a':1, 'b':2, 'c':None } # création d’un dictionnaire
    >>> dict
    {'a': 1, 'c': None, 'b': 2} # les éléments d’un dictionnaire ne sont pas ordonnés

    >>> dict['b'] # accès à la valeur ayant pour clef 'b'
    2

    >>> dict['c'] = 3 # modification de la valeur correspondant à la clef 'c'
    >>> dict['c']
    3

    >>> dict['d'] = 4 # insertion d’un élément
    >>> 'd' in dict # la clef 'd' est-elle dans dic ?
    True

    >>> del dict['a'] # supprime l’élément ayant pour clef 'a'
    >>> dict
    {'b': 2, 'c': 3, 'd': 4}

    >>> dict.keys() # retourne la liste des clefs
    ['b', 'c', 'd']

    >>> dict.values() # retourne la liste des valeurs
    [2, 3, 4]

    >>> dict.items() # retourne la liste des couples (clef, valeur)
    [('b', 2), ('c', 3), ('d',4)]

    >>> dict[6] # retourne une exception si clef erronée
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    KeyError: 6

    >>> dict.get(6) # retourne None
    ```

!!!note "Accès aux éléments : comparaison entre les listes et les dictionnaires."
    | Opération | Pour une liste | Pour un dictionnaire |
    | -----: | :-----------   | :--------  |
    |Initialisation |` liste = [1, 2, 3, 9, 6] ` | `dico = {'A':2 , 'B':5, 'C':9}` | 
    |Accès à un élément| `valeur = liste[3]` | `valeur = dico['C']`|
    |Modification d'un élément | `liste[2] = 45` | `dico['B'] = 23` |
    |Ajout d'un élément | `liste.append(36)` | `dico['D'] = 7` |
    |Longueur | `len(liste)` | `len(dico)` |
    |Savoir si un élément est présent | `if valeur in liste` | `if valeur in dico.values()` |
    |Savoir si une clé est présente | - | `if clef in dico.keys():` |
    |Itérer sur les clefs | - | `for clef in dico.keys():` |
    |Itérer sur les valeurs | - | `for valeur in dico.values():` |
    |Itérer sur les clefs et valeurs | - | `for clef, valeur in dico.items():` |
