from flask import Flask, render_template, request
from random import randint

app = Flask(__name__)
personnes = [{'nom' : 'TURING', 'prenom' : 'Alan'}, {'nom' : 'HILBERT', 'prenom' : 'David'}, {'nom' : 'NEWTON', 'prenom' : 'Isaac'}, {'nom' : 'POINCARRÉ', 'prenom' : 'Raymond'}]

# Racine
@app.route('/', methods = ['GET', 'POST'])
def fonction_racine():
    # Nous indiquons que lors de l'appel de cette fonction, les
    # paramètres peuvent être fournis à l'aide des deux méthodes
    global personnes
    
    if request.method == 'GET':
        # Lors du premier appel de la fonction, aucun paramètre n'est fourni
        # par défaut cela se fait par la méthode POST
        return render_template('index.html', personnes = personnes)
    else:
        if request.form['validation'] == 'Supprimer':
            taille_tableau = len(personnes)
            k = 0
            for i in range(taille_tableau):
                # Retourne True si la case d'id i a été cochée
                if request.form.get(f'{i}'):
                    # On supprime la personne de la liste
                    personnes.pop(i - k)
                    # On décale de 1 les éléments de la liste vers la gauche
                    k = k + 1
        elif request.form['validation'] == 'Ajouter':
            # On vérifie que la zone de saisie est bien complété
            if request.form.get('nom') and request.form.get('prenom'):
                personnes.append({'nom' : request.form['nom'], 'prenom' : request.form['prenom']})
        return render_template('index.html', personnes = personnes)
    

if __name__ == "__main__":
    app.run(debug = True)