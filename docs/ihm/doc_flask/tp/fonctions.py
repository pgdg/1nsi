def cesar(phrase : str, cle : int) -> str:
    """ 
    Cette fonction retourne la chaîne de caractères obtenue par la 
    méthode de César appliquée sur la chaîne phrase avec la clé cle
    param phrase (str) : chaîne de caractères à crypter, sans 
                         caractères spéciaux, ni ponctuation 
                         ou accents, les espaces sont conservés
    param cle (int) : clé utilisée par la méthode de César
    return (str) : chaîne de caractères résultat du codage
    """
    phrase_crypte = ""
    for caractere in phrase:
        valeur_caractere = ord(caractere)
        if valeur_caractere != 32: # Espace
            # Il y a 58 caractères de A à z dans la table ASCII
            valeur_crypte = (valeur_caractere + cle - 65) % 58 + 65
        else:
            valeur_crypte = 32
        phrase_crypte = phrase_crypte + chr(valeur_crypte)
    return phrase_crypte