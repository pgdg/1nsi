from flask import Flask

# On crée une instance de la classe Flask que l'on vient d'importer ci-dessus
app = Flask(__name__)

# On déclare la route de la racine du site et les fonctions qui seront appelées
@app.route('/')
def fonction_1():
    return "Hello world !"

# Lorsque l'on interprète ce ficher avec Python, la méthode run() est exécutée
# Nous verrons par la suite les paramètres que l'on peut indiquer à cette méthode
if __name__ == "__main__":
    app.run()