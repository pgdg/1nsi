from flask import Flask, render_template
from random import randint

app = Flask(__name__)

@app.route('/')
def fonction_alea():
    # On génère le tableau par compréhension
    return render_template("index.html", tableau = [randint(1, 100) for i in range(50)])

if __name__ == "__main__":
    app.run(debug = True)