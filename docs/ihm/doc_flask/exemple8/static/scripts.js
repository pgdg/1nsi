function calcul(operation) {
    // Définition des données à transmettre et de la méthode utilisée
    const data = {
        method: "POST",
        // Les données sont récupérées dans la page 
        body: new URLSearchParams({
                    nombre: document.getElementById('nombre').value,
                    operation: operation,
                    })
    };
    // Voici la méthode permettant l'échange de données avec le serveur (allé et retour)
    // data est défini juste au dessus et contient notamment "nombre" et "operation"       
    fetch("/", data)
        .then(
            // Attente d'une réponse du serveur sous forme d'un object JSON (équivalent du dictionnaire en python)
            // le nom de la variable "promesse" n'a pas d'importance
            promesse => promesse.json()
        )
        .then( 
            // Traitement de la réponse du serveur
            // le nom de la variable "reponse" n'a pas d'importance
            reponse => document.getElementById('nombre').value = reponse["resultat"]
        )
        .catch(
            // On peut gérer l'erreur si tel est le cas (par exemple si le champs clé est vide)
            error => alert("Erreur : " + error)
        );                
}